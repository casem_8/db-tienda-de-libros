-- Creacion de la base de datos

CREATE DATABASE TiendaDeLibros
USE TiendaDeLibros

-- Tabla de Clientes
CREATE TABLE Clientes (
    IdCliente INT PRIMARY KEY IDENTITY NOT NULL,
    Nombre VARCHAR(255) NOT NULL,
    Correo VARCHAR(255) NOT NULL,
    Direccion VARCHAR(255) NOT NULL,
	Clave VARCHAR(255) NOT NULL
);

-- Tabla de Libros
CREATE TABLE Libros (
    IdLibro INT PRIMARY KEY IDENTITY NOT NULL,
    Titulo VARCHAR(255) NOT NULL,
    Descripcion TEXT NOT NULL,
    Precio DECIMAL(10, 2) NOT NULL,
    Stock INT NOT NULL
);

-- Tabla de Pedidos
CREATE TABLE Pedidos (
    IdPedido INT PRIMARY KEY IDENTITY NOT NULL,
    IdCliente INT,
    FechaPedido DATE NOT NULL,
	CONSTRAINT FK_Cliente FOREIGN KEY (IdCliente) REFERENCES Clientes(IdCliente)
);

-- Tabla de Detalles de Pedido
CREATE TABLE DetallesPedido (
    IdDetalles INT PRIMARY KEY IDENTITY NOT NULL,
    IdPedido INT,
    IdLibro INT,
    Cantidad INT NOT NULL,
    PrecioUnitario DECIMAL(10, 2) NOT NULL,
	CONSTRAINT FK_Pedido_Detalles FOREIGN KEY (IdPedido) REFERENCES Pedidos(IdPedido),
	CONSTRAINT FK_Libro_Detalles FOREIGN KEY (IdLibro) REFERENCES Libros(IdLibro)
);