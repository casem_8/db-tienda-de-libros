-- Sentencia while utilizada para recorrer la tabla Clientes y mostrar el nombre del cliente

USE TiendaDeLibros;

-- Declaramos las variables
DECLARE @Cantidad INT = (SELECT COUNT(*) FROM Clientes);
DECLARE @Texto VARCHAR(50);
DECLARE @Contador INT = 0;

-- Iniciamos la sentencia While
WHILE (@Contador < = @Cantidad)

BEGIN
	SET @Texto = (SELECT Nombre FROM Clientes WHERE IdCliente = @Contador);
	PRINT 'Nombre del cliente ' + CONVERT(VARCHAR(20), @Contador) + ' ' + @Texto;
	SET @Contador = @Contador + 1
END

