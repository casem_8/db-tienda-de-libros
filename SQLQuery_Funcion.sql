-- Funci�n para calcular el total de venta para un cliente espec�fico

USE TiendaDeLibros;

CREATE FUNCTION CalcularTotalPedido
(
    @IdPedido INT
)
RETURNS DECIMAL(10, 2)
AS
BEGIN
    DECLARE @TotalPedido DECIMAL(10, 2);

    SELECT @TotalPedido = SUM(DP.Cantidad * DP.PrecioUnitario)
    FROM DetallesPedido DP
    WHERE DP.IdPedido = @IdPedido ;

    RETURN @TotalPedido;
END;



DECLARE @TotalPedido DECIMAL(10, 2);
SET @TotalPedido = dbo.CalcularTotalPedido(1);

PRINT 'El total del pedido es: ' + CAST(@TotalPedido AS VARCHAR(20));