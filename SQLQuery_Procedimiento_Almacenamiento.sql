-- Procedimiento almacenado para listar los libros con un stock bajo

USE TiendaDeLibros;

CREATE PROCEDURE ObtenerLibrosConStockBajo
    @StockMinimo INT
AS
BEGIN
    SELECT IdLibro, Titulo, Stock FROM Libros
    WHERE Stock < @StockMinimo;
END;

EXEC ObtenerLibrosConStockBajo @StockMinimo = 10;
