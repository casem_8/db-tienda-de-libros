-- Cursor para recorrer la tabla de libros y verificar que el stock de cada libro no sea negativo
USE TiendaDeLibros;

-- Variables
DECLARE @IdLibro INT;
DECLARE @Titulo VARCHAR(255);
DECLARE @Stock INT;

-- Declaramos el cursor
DECLARE CursorLibro CURSOR FOR
SELECT IdLibro, Titulo, Stock FROM Libros;

-- Iniciamos el cursor
OPEN CursorLibro;

	-- Recorremos el cursor
	FETCH NEXT FROM CursorLibro INTO @IdLibro, @Titulo, @Stock;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Realizamos la validación del stock
		IF @Stock < 0
		BEGIN
			PRINT 'Hay un error en el libro ' + @Titulo + ': El stock no puede ser negativo.';
		END

		-- Obtenemos el siguiente libro
		FETCH NEXT FROM CursorLibro INTO @IdLibro, @Titulo, @Stock;
END;

-- Cerramos el cursor
CLOSE CursorLibro;

DEALLOCATE CursorLibro;