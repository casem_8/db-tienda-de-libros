-- Vista para mostrar los detalles de pedidos para un cliente espec�fico seg�n su id
USE TiendaDeLibros;

CREATE VIEW VistaDetallesPedidosCliente 
AS
SELECT P.IdCliente, P.FechaPedido,
       DP.IdLibro, DP.Cantidad, DP.PrecioUnitario
FROM Pedidos P

INNER JOIN DetallesPedido DP ON P.IdPedido = DP.IdPedido;


SELECT * FROM VistaDetallesPedidosCliente;

SELECT * FROM VistaDetallesPedidosCliente
WHERE IdCliente = 3;