-- Insertando datos en la base de datos TiendaDeLibros
USE TiendaDeLibros;

INSERT INTO Libros (Titulo, Descripcion, Precio, Stock)
VALUES
	('Mil soles espl�ndidos', 'Libro escrito por Khaled Hosseini', 35000.00, 10),
	('Cumbres borrascosas', 'Libro escrito por Emily Bront�', 45000.00, 8),
	('El retrato de Dorian Gray', 'Libro escrito por Oscar Wilde', 25000.00, 12),
	('El amor en los tiempo de colera', 'Libro escrito por Gabriel Garcia Marquez', 32000.00, 20);


INSERT INTO Clientes (Nombre, Correo, Direccion, Clave)
VALUES
	('Pedro Ramirez', 'pedro.ramirez@correo.com', 'calle 1', '123'),
	('Mar�a Castro', 'maria.castro@correo.com', 'calle 2', '123'),
	('Marcela Ortiz', 'marcela.ortiz@correo.com', 'calle 3', '123'),
	('Juan Lopez', 'juan.lopez@correo.com', 'calle 4', '123');


INSERT INTO Pedidos (IdCliente, FechaPedido)
VALUES
	(1,'2023-09-14'),
	(2, '2023-09-15'),
	(3, '2023-09-16'),
	(4, '2023-09-17');


INSERT INTO DetallesPedido (IdPedido, IdLibro, Cantidad, PrecioUnitario)
VALUES
	(1, 2, 2, 45000.00),
	(2, 1, 1, 35000.00),
	(3, 3, 3, 25000.00),
	(4, 1, 2, 32000.00);


SELECT * FROM Clientes;

